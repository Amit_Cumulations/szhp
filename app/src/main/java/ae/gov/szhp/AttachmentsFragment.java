package ae.gov.szhp;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Amit T on 23-09-2017.
 */

public class AttachmentsFragment extends Fragment {
    // 1. First, we declare the layout that was included as a View objects.
    @BindView( R.id.licenseAttachmentLayout ) View licenseAttachmentLayout;
    @BindView( R.id.memberAttachmentLayout ) View memberAttachmentLayout;
    @BindView( R.id.authAttachmentLayout ) View authAttachmentLayout;
    @BindView( R.id.facilityAttachmentLayout ) View facilityAttachmentLayout;

    public AttachmentsFragment() {
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        View view = inflater.inflate(R.layout.fragment_attachments,container,false);

        // 2. In here, we bind the included layouts
        ButterKnife.bind(this,view);

        // 4. Then, we create objects of the type of the IncludedLayout.
        //      In this example the layout reuse the same layout twice, so, there are two
        //      IncludedLayouts.
        IncludedLayout includedLayout_1 = new IncludedLayout();
        IncludedLayout includedLayout_2 = new IncludedLayout();
        IncludedLayout includedLayout_3 = new IncludedLayout();
        IncludedLayout includedLayout_4 = new IncludedLayout();

        // 5. We bind the elements of the included layouts.
        ButterKnife.bind(includedLayout_1,licenseAttachmentLayout);
        ButterKnife.bind(includedLayout_2,memberAttachmentLayout);
        ButterKnife.bind(includedLayout_3,authAttachmentLayout);
        ButterKnife.bind(includedLayout_4,facilityAttachmentLayout);

        includedLayout_1.attachmentNameTv.setText(getString(R.string.license_attachment));
        includedLayout_2.attachmentNameTv.setText(getString(R.string.membership_attachment));
        includedLayout_3.attachmentNameTv.setText(getString(R.string.auth_attachment));
        includedLayout_4.attachmentNameTv.setText(getString(R.string.facility_attachment));

        return view;
    }

    // 3. We create a static class that will be an container of the elements
    //     of the included layout. In here we declare the components that
    //     hold this. In this example, there is only one TextView.
    static class IncludedLayout {
        @BindView( R.id.attachmentTv )
        TextView attachmentNameTv;
        @BindView( R.id.attachmentPreviewIv )
        ImageView attachmentPreviewIv;
    }
}
