package ae.gov.szhp;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

/**
 * Created by Amit T on 21-09-2017.
 */

public class BankDetailsFragment extends Fragment {

    public BankDetailsFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_bank_details, container, false);
        Spinner bankNameSpinner = (Spinner) view.findViewById(R.id.bankNameSpinner);
        Spinner bankBranchSpinner = (Spinner) view.findViewById(R.id.bankBranchSpinner);
// Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getActivity(),
                R.array.values_array, android.R.layout.simple_spinner_item);
// Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
// Apply the adapter to the spinner
        bankNameSpinner.setAdapter(adapter);
        bankBranchSpinner.setAdapter(adapter);
        return view;
    }
}
